export interface Client {
    name: string
    description?: String;
    date?: String;
    type?: String;
}
