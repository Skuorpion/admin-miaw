import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../services/clients.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {
  form: FormGroup;
  name = '';

  constructor(private clientsService: ClientsService, private fb: FormBuilder, private router: Router) { }

  add() {
    this.clientsService.addClient(this.form.value).subscribe(response => {
      this.router.navigate(['/clients']);
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: [''],
      date: [''],
      type: ['']
    });
  }

}
